package com.procyk.maciej.android.pocketcubesolver.camera

import android.graphics.Bitmap
import android.graphics.Color.*
import android.util.Log
import com.procyk.maciej.android.pocketcubesolver.cube.adapter.toCubeColor
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeColor
import java.lang.Integer.min
import kotlin.math.abs

class Photo(val bitmap: Bitmap, val checkDistance: Int = 8, val isCentered: Boolean = false) {
    val sizeX: Int
    val sizeY: Int

    init {
        sizeX = bitmap.width
        sizeY = bitmap.height
    }

    fun getColorAt(location: Pair<Int, Int>): CubeColor {
        if (!location.isOnImageSize(bitmap))
            return CubeColor.NOT_SET

        val strengths = mutableMapOf<CubeColor, Double>()

        strengths.merge(bitmap.getColorAt(location).toCubeColor(), checkDistance + 1.0, Double::plus)
        for (curDist in 1..checkDistance) {
            location.getNeighboursAtDistance(curDist)
                .forEach {
                    if (it.isOnImageSize(bitmap))
                        strengths.merge(bitmap.getColorAt(it).toCubeColor(), getWeightAt(curDist,checkDistance), Double::plus)
                }
        }
        val ranges = logColors(bitmap, checkDistance, location)
        Log.i("COLOR_MAP", strengths.map { it.key.name + " : " + it.value.toString() } .joinToString())
        return strengths
            .filter { it.key != CubeColor.NOT_SET }
            .maxBy { it.value } ?.key ?: throw ColorNotDetectedException(ranges)
    }

    private fun getWeightAt(currentDistance: Int, checkDistance: Int): Double {
        val howManyAtDistance = 8 * currentDistance;
        val sumWeight = checkDistance - currentDistance + 1.0
        return sumWeight / howManyAtDistance;
    }

    fun getPositionsOfCenters(dimension: Int): List<Pair<Int, Int>> {
        val X = if (isCentered) min(sizeX, sizeY) else sizeX
        val Y = if (isCentered) X else sizeY
        val shift = Pair((sizeX - X) / 2, (sizeY - Y) / 2)

        val result = mutableListOf<Pair<Int, Int>>()
        for (y in 1..dimension)
            for (x in 1..dimension) {
                val singleX = X / dimension
                val singleY = Y / dimension
                result.add(Pair((x * singleX) - (singleX / 2), (y * singleY) - (singleY / 2)))
            }
        return result.map { it + shift }
    }
}

class ColorNotDetectedException(val description: String) : Exception(description)

private fun logColors(bitmap: Bitmap, checkDistance: Int, location: Pair<Int, Int>): String {
    val R = mutableSetOf<Int>()
    val G = mutableSetOf<Int>()
    val B = mutableSetOf<Int>()
    for (curDist in 1..checkDistance) {
        location.getNeighboursAtDistance(curDist).forEach {
            if (it.isOnImageSize(bitmap)) {
                val c = bitmap.getColorAt(it)
                R.add(red(c))
                G.add(green(c))
                B.add(blue(c))
            }
        }
    }
    val colorRanges = "R_MIN=" + R.min() + "__R_MAX=" + R.max() + "__G_MIN=" + G.min() + "__G_MAX=" + G.max() + "__B_MIN=" + B.min() + "__B_MAX=" + B.max()
    Log.i("Color pick", colorRanges)
    return colorRanges
}

operator fun Pair<Int, Int>.plus(inc: Pair<Int, Int>) = Pair(this.first+inc.first, this.second+inc.second)

fun Pair<Int, Int>.getNeighboursAtDistance(distance: Int): List<Pair<Int, Int>> {

    fun ArrayList<Int>.addPart(distance: Int) {
        val rng1 = 1..(2 * abs(distance) + 1)
        val rng2 = (1 - abs(distance) until abs(distance))
        for (i in rng1)
            this.add(distance)
        for (i in if (distance > 0) rng2.reversed() else rng2)
            this.add(i)
    }
    require(distance > 0)
    val support = ArrayList<Int>()
    support.addPart(-distance)
    support.addPart(distance)

    return List(support.size)
    { i -> Pair(this.first + support[i],
                this.second + support[(i + 2 * distance) % support.size]) }
}

fun Bitmap.getColorAt(location: Pair<Int, Int>): Int
        = this.getPixel(location.first, location.second)

fun Pair<Int, Int>.isOnImageSize(bitmap: Bitmap) : Boolean
        = this.first > -1 && this.first < bitmap.width && this.second > -1 && this.second < bitmap.height