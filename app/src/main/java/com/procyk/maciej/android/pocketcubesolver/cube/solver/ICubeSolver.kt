package com.procyk.maciej.android.pocketcubesolver.cube.solver

import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeMove
import java.lang.Exception

interface ICubeSolver {
    @Throws(NoSolutionFound::class)
    fun findSolution(cube: Cube): Array<CubeMove>

    class NoSolutionFound : Exception()
}