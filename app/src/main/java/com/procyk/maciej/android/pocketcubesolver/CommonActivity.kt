package com.procyk.maciej.android.pocketcubesolver

import android.support.v7.app.AppCompatActivity
import android.widget.Toast

abstract class CommonActivity : AppCompatActivity() {

    fun toastMessage(message: String, duration: Int = Toast.LENGTH_SHORT)
            = Toast.makeText(this, message, duration).show()
}