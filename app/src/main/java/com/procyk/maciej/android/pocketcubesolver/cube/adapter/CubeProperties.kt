package com.procyk.maciej.android.pocketcubesolver.cube.adapter

import android.graphics.Color.*
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeColor
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeFace

fun CubeColor.getNextCandidate(): CubeColor {
    val values = CubeColor.values()
    val index = (this.ordinal + 1) % values.size
    return values[index]
}

fun CubeFace.getNextFace(): CubeFace {
    val values = CubeFace.values()
    val index = (this.ordinal + 1) % values.size
    return values[index]
}

private fun typedRanges(color: CubeColor) =
    when(color) {
        CubeColor.WHITE -> Triple(170..245, 150..230, 115..225)
        CubeColor.YELLOW -> Triple(170..235, 140..230, 0..60)
        CubeColor.BLUE -> Triple(20..80, 40..105, 70..150)
        CubeColor.GREEN -> Triple(20..110, 80..160, 30..100)
        CubeColor.ORANGE -> Triple(150..255, 60..165, 0..50)
        CubeColor.RED -> Triple(150..210, 0..150, 0..100)
        CubeColor.NOT_SET -> Triple(0..0, 0..0, 0..0)
}

fun Int.toCubeColor(ranger: (CubeColor) -> Triple<IntRange, IntRange, IntRange> = ::typedRanges): CubeColor {
    val R = red(this)
    val G = green(this)
    val B = blue(this)
    for (color in CubeColor.definedColors()) {
        val rng = ranger(color)
        if (R in rng.first && G in rng.second && B in rng.third)
            return color
    }
    return CubeColor.NOT_SET
}