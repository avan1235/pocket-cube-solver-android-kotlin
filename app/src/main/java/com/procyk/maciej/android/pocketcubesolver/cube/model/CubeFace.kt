package com.procyk.maciej.android.pocketcubesolver.cube.model

enum class CubeFace {
    UP {
        override fun getFaceModShift(): Int = 0
        override fun getToNextDirection() = ScanDirection.UP
        override fun indexStart(): Int = 8
    },
    FRONT {
        override fun getFaceModShift(): Int = 0
        override fun getToNextDirection() = ScanDirection.LEFT
        override fun indexStart(): Int = 0
    },
    RIGHT {
        override fun getFaceModShift(): Int = 0
        override fun getToNextDirection() = ScanDirection.UP
        override fun indexStart(): Int = 4
    },
    DOWN {
        override fun getFaceModShift(): Int = 1
        override fun getToNextDirection() = ScanDirection.LEFT
        override fun indexStart(): Int = 12
    },
    BACK {
        override fun getFaceModShift(): Int = 1
        override fun getToNextDirection() = ScanDirection.UP
        override fun indexStart(): Int = 16
    },
    LEFT {
        override fun getFaceModShift(): Int = 2
        override fun getToNextDirection() = ScanDirection.LEFT
        override fun indexStart(): Int = 20
    };

    abstract fun getToNextDirection(): ScanDirection
    abstract fun getFaceModShift(): Int
    abstract fun indexStart(): Int

    fun nextScanFace(): CubeFace = CubeFace.values()[(this.ordinal + 1) % values().size]

    enum class ScanDirection {
        UP,
        LEFT
    }
}