package com.procyk.maciej.android.pocketcubesolver.cube.solver

import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeMove
import java.lang.Exception

class DFSSolver : ICubeSolver {

    companion object {
        private val POSSIBLE_MOVES = CubeMove.values()
        private val NUMBER_OF_POSSIBLE_MOVES = POSSIBLE_MOVES.size
        private const val LONGEST_POSSIBLE_SOLUTION = 11

        @Throws(Solution::class)
        private fun findSolutionHelp(currentDepth: Int, cube: Cube, moves: Array<CubeMove?>, solver: DFSSolver) {
            if (cube.isSolved()) throw Solution(currentDepth, moves)
            if (currentDepth >= solver.getLengthOfLongestSolution()) return

            for (currentMove in 0 until NUMBER_OF_POSSIBLE_MOVES) {
                if (
                    (currentDepth == 0 ||
                        ((moves[currentDepth-1] != POSSIBLE_MOVES[currentMove].getReverse())
                            && (moves[currentDepth-1] != POSSIBLE_MOVES[currentMove].getCompletion())
                            && (moves[currentDepth-1]?.getCompletion() != POSSIBLE_MOVES[currentMove])
                            && (moves[currentDepth-1] != POSSIBLE_MOVES[currentMove] || POSSIBLE_MOVES[currentMove] == POSSIBLE_MOVES[currentMove].getReverse())))
                    && (currentDepth < 4 ||
                        moves[currentDepth-3] != moves[currentDepth-2] || moves[currentDepth-2] != moves[currentDepth-1] || moves[currentDepth-1] != POSSIBLE_MOVES[currentMove])
                ) {
                    moves[currentDepth] = POSSIBLE_MOVES[currentMove]
                    cube.move(POSSIBLE_MOVES[currentMove])
                    findSolutionHelp(currentDepth+1, cube, moves, solver)
                    cube.move(POSSIBLE_MOVES[currentMove].getReverse())
                }
            }
        }
    }

    override fun findSolution(cube: Cube): Array<CubeMove> {
        try {
            val moves = Array<CubeMove?> (getLengthOfLongestSolution()) { null }
            findSolutionHelp(0, cube, moves, this)
            throw ICubeSolver.NoSolutionFound()
        } catch (exc: Solution) {
            val filtered = exc.solution.filterNotNull()
            if (filtered.size == exc.solution.size)
                return filtered.toTypedArray()
            else throw ICubeSolver.NoSolutionFound()
        }
    }

    fun getLengthOfLongestSolution(): Int = LONGEST_POSSIBLE_SOLUTION

    private class Solution(currentDepth: Int, savedMoves: Array<CubeMove?>) : Exception() {
        val solution: Array<CubeMove?>

        init {
            solution = Array(currentDepth) { i -> savedMoves[i] }
        }

        override fun toString(): String = this.solution.joinToString(separator = "->")
    }
}