package com.procyk.maciej.android.pocketcubesolver.cube.model

enum class CubeMove(val permutations: Array<IntArray>) {
    F(arrayOf(intArrayOf(0,3,2,1),intArrayOf(10,20,12,7),intArrayOf(11,21,13,4))) {
        override fun getReverse() = G
        override fun getCompletion() = H
        override fun toString() = "F"
    },
    G(arrayOf(intArrayOf(3,0,1,2),intArrayOf(20,10,7,12),intArrayOf(21,11,4,13))) {
        override fun getReverse() = F
        override fun getCompletion() = H
        override fun toString() = "F'"
    },
    H(arrayOf(intArrayOf(3,1),intArrayOf(2,0),intArrayOf(20,7),intArrayOf(12,10),intArrayOf(21,4),intArrayOf(13,11))) {
        override fun getReverse() = H
        override fun getCompletion() = null
        override fun toString() = "F2"
    },
    R(arrayOf(intArrayOf(4,7,6,5),intArrayOf(1,13,17,9),intArrayOf(2,14,18,10))) {
        override fun getReverse() = T
        override fun getCompletion() = Y
        override fun toString() = "R"
    },
    T(arrayOf(intArrayOf(7,4,5,6),intArrayOf(13,1,9,17),intArrayOf(14,2,10,18))) {
        override fun getReverse() = R
        override fun getCompletion() = Y
        override fun toString() = "R'"
    },
    Y(arrayOf(intArrayOf(7,5),intArrayOf(6,4),intArrayOf(13,9),intArrayOf(17,1),intArrayOf(14,10),intArrayOf(18,2))) {
        override fun getReverse() = Y
        override fun getCompletion() = null
        override fun toString() = "R2"
    },
    U(arrayOf(intArrayOf(8,11,10,9),intArrayOf(0,4,18,23),intArrayOf(1,5,19,20))) {
        override fun getReverse() = I
        override fun getCompletion() = O
        override fun toString() = "U"
    },
    I(arrayOf(intArrayOf(11,8,9,10),intArrayOf(4,0,23,18),intArrayOf(5,1,20,19))) {
        override fun getReverse() = U
        override fun getCompletion() = O
        override fun toString() = "U'"
    },
    O(arrayOf(intArrayOf(11,9),intArrayOf(10,8),intArrayOf(4,23),intArrayOf(18,0),intArrayOf(5,20),intArrayOf(19,1))) {
        override fun getReverse() = O
        override fun getCompletion() = null
        override fun toString() = "U2"
    };

    abstract fun getReverse(): CubeMove
    abstract override fun toString(): String
    abstract fun getCompletion(): CubeMove?

    fun toByte(): Byte = this.ordinal.toByte()

    companion object {
        fun fromChar(char: Char): CubeMove = valueOf(char.toString())

        fun fromByte(inputData: Byte): CubeMove = CubeMove.values()[inputData.toInt()]
    }
}