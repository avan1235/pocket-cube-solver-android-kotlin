package com.procyk.maciej.android.pocketcubesolver.cube.adapter

import android.view.View
import com.procyk.maciej.android.pocketcubesolver.R
import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeColor

fun Cube.applyCurrentViewFor(views: Collection<View>): Unit =
    views.forEachIndexed { index, view -> view.setBackgroundResource(
        when(this.getColorAt(index)) {
            CubeColor.WHITE -> R.drawable.button_white_bg
            CubeColor.ORANGE -> R.drawable.button_orange_bg
            CubeColor.GREEN -> R.drawable.button_green_bg
            CubeColor.RED -> R.drawable.button_red_bg
            CubeColor.YELLOW -> R.drawable.button_yellow_bg
            CubeColor.BLUE -> R.drawable.button_blue_bg
            CubeColor.NOT_SET -> R.drawable.button_empty_bg
        }
    ) }



