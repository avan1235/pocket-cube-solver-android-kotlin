package com.procyk.maciej.android.pocketcubesolver.camera

import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeColor

interface CubeFaceRecognizer {

    fun scanFace(inputPhoto: Photo): List<CubeColor>
}
