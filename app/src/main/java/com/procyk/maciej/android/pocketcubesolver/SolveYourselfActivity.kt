package com.procyk.maciej.android.pocketcubesolver

import android.os.Bundle
import android.view.View
import android.widget.Button
import com.procyk.maciej.android.pocketcubesolver.cube.adapter.applyCurrentViewFor
import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeMove

import kotlinx.android.synthetic.main.activity_solve_yourself.*
import kotlinx.android.synthetic.main.buttons_layout.*
import kotlinx.android.synthetic.main.cube_layout.*
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import kotlinx.android.synthetic.main.activity_solve_yourself.navigationView
import kotlinx.android.synthetic.main.activity_solve_yourself.toolbar

class SolveYourselfActivity : DrawerLayoutActivity() {

    private val cube: Cube = Cube.getSolved()
    private val cubeButtons: ArrayList<Button> = ArrayList()

    override val toolbarUsed: Toolbar
        get() = this.toolbar
    override val layoutResIdUsed: Int
        get() = R.layout.activity_solve_yourself
    override val drawerLayoutUsed: DrawerLayout
        get() = this.drawerLayout
    override val navigationViewUsed: NavigationView
        get() = this.navigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cubeButtons.addAll(arrayListOf(
                button0,button1,button2,button3,
                button4,button5,button6,button7,
                button8,button9,button10,button11,
                button12,button13,button14,button15,
                button16,button17,button18,button19,
                button20,button21,button22,button23
            ))

        cubeButtons.forEach { it.isEnabled = false }

        this.buttonF.setOnClickListener(createMoveAndUpdateFunction(CubeMove.F))
        this.buttonFP.setOnClickListener(createMoveAndUpdateFunction(CubeMove.G))
        this.buttonR.setOnClickListener(createMoveAndUpdateFunction(CubeMove.R))
        this.buttonRP.setOnClickListener(createMoveAndUpdateFunction(CubeMove.T))
        this.buttonU.setOnClickListener(createMoveAndUpdateFunction(CubeMove.U))
        this.buttonUP.setOnClickListener(createMoveAndUpdateFunction(CubeMove.I))

        this.refreshButton.setOnClickListener { shuffleOrSolveCube() }
        this.restartButton.setOnClickListener { resetCubeView() }

        this.showCurrentCubeView()
    }

    private fun shuffleOrSolveCube() {
        if (!this.cube.isSolved())
            this.goToSolvingActivity()
        else
            this.shuffleCubeAndUpdateView()
    }

    private fun goToSolvingActivity() {
        this.createAndRunIntent(CubeSolverActivity::class.java, "CUBE_TO_SOLVE" to this.cube.toIntArray())
    }

    private fun shuffleCubeAndUpdateView(): Unit {
        this.cube.shuffle()
        this.showCurrentCubeView()
    }

    private fun createMoveAndUpdateFunction(move: CubeMove): ((v: View) -> Unit) {
        return {
            this.cube.move(move)
            this.showCurrentCubeView()
        }
    }

    private fun checkCurrentCubeStateForButton() {
        this.refreshButton.setImageResource(
            if (this.cube.isSolved())
                R.drawable.refresh_icon
            else
                R.drawable.send_icon
        )
    }

    private fun showCurrentCubeView() {
        this.cube.applyCurrentViewFor(this.cubeButtons)
        this.checkCurrentCubeStateForButton()
    }

    private fun resetCubeView() {
        this.cube.setSolved()
        this.showCurrentCubeView()
    }
}
