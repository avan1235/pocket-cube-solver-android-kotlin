package com.procyk.maciej.android.pocketcubesolver.camera

import android.media.FaceDetector
import android.util.Log
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeColor
import java.lang.Exception
import java.lang.IllegalArgumentException

class SizedCubeFaceRecognizer : CubeFaceRecognizer {

    override fun scanFace(inputPhoto: Photo): List<CubeColor> {
        val centers = inputPhoto.getPositionsOfCenters(2)
        var scanSuccess = true
        val errorDescription = StringBuilder()
        val result = Array(4) { index ->
            try {
                inputPhoto.getColorAt(centers[index.mapAsCubeFaceIndex()])
            } catch (e: ColorNotDetectedException) {
                scanSuccess = false
                errorDescription.append(index.mapAsCubeFaceIndex()).append("___").append(e.description).append("____")
                return@Array CubeColor.NOT_SET
            }}
        val scannedColorsDescription = result.map { it.name }.toList().joinToString(separator="_")
        Log.i("Scanned colors", scannedColorsDescription)
        errorDescription.append(scannedColorsDescription)
        return if (scanSuccess) result.toList()
        else throw FaceNotDetectedException(errorDescription.toString())
    }

    private fun Int.mapAsCubeFaceIndex(): Int
            = when(this) {
                0 -> 2
                1 -> 0
                2 -> 1
                3 -> 3
                else -> throw IllegalArgumentException("Pocket cube has only four parts on face")
            }
}

class FaceNotDetectedException(val description: String) : Exception(description)

