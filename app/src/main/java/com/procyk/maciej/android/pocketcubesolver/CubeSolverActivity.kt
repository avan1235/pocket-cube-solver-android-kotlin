package com.procyk.maciej.android.pocketcubesolver

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.procyk.maciej.android.pocketcubesolver.cube.adapter.applyCurrentViewFor
import com.procyk.maciej.android.pocketcubesolver.cube.adapter.getNextCandidate
import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeColor
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeMove
import com.procyk.maciej.android.pocketcubesolver.cube.solver.DFSSolver
import com.procyk.maciej.android.pocketcubesolver.cube.solver.ICubeSolver
import com.procyk.maciej.android.pocketcubesolver.utils.*
import kotlinx.android.synthetic.main.activity_cube_solver.*
import kotlinx.android.synthetic.main.cube_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.io.InputStream
import java.io.InterruptedIOException
import java.io.OutputStream
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.collections.ArrayList

class CubeSolverActivity : DrawerLayoutActivity() {

    private val cube: Cube = Cube.getEmpty()
    private val solver: ICubeSolver = DFSSolver()

    private val cubeButtons: ArrayList<Button> = ArrayList()

    override val drawerLayoutUsed: DrawerLayout
        get() = this.drawerLayout
    override val layoutResIdUsed: Int
        get() = R.layout.activity_cube_solver
    override val navigationViewUsed: NavigationView
        get() = this.navigationView
    override val toolbarUsed: Toolbar
        get() = this.toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cubeButtons.addAll(arrayOf(
            button0,button1,button2,button3,
            button4,button5,button6,button7,
            button8,button9,button10,button11,
            button12,button13,button14,button15,
            button16,button17,button18,button19,
            button20,button21,button22,button23
        ))

        this.intent.getIntArrayExtra("CUBE_TO_SOLVE")?.let { this.cube.fromIntArray(it) }
        cubeButtons.forEachIndexed { index, button -> button.setOnClickListener(getChangeColorListener(index)) }

        this.buttonReset.setOnClickListener { this.resetCube() }
        this.buttonSolve.setOnClickListener { GlobalScope.launch (Dispatchers.Main) { findSolutionAndShow() } }

        this.refreshCubeView()
    }

    private fun resetCube() {
        for (i in 0 until Cube.CUBE_FIELDS)
            this.cube.setColor(i, CubeColor.NOT_SET)
        this.solutionText.text = getString(R.string.createCube)
        this.refreshCubeView()
    }

    private fun getChangeColorListener(index: Int): ((v: View) -> Unit) {
        return {
            val newState = getNextState(index)
            this.cube.setColor(index, newState)
            this.refreshCubeView()
        }
    }

    private fun getNextState(index: Int): CubeColor = cube.getColorAt(index).getNextCandidate()

    private fun refreshCubeView() {
        this.cube.applyCurrentViewFor(this.cubeButtons)
    }

    private fun setBeforeSearching() {
        this.refreshCubeView()
        this.solutionText.text = getString(R.string.searchBackground)
    }

    private fun setSolution(description: String) {
        this.solutionText.text = description
    }

    private suspend fun findSolutionAndShow() {
        if (!cube.isCorrectlyFilled()) {
            Toast.makeText(this, "Fill cube correctly", Toast.LENGTH_LONG).show()
            return
        }
        setBeforeSearching()
        val startedWith = cube.copy()
        val solution = solveInAnotherThread(cube.copy())
        setSolution(if (startedWith == this.cube) solution.joinToString(separator = " ➭ ") else getString(R.string.changedCube))
    }

    private suspend fun solveInAnotherThread(cube: Cube): Array<CubeMove> =
        withContext(Dispatchers.Default) {
            return@withContext solver.findSolution(cube)
        }
}