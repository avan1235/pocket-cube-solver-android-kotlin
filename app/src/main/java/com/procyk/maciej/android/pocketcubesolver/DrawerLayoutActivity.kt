package com.procyk.maciej.android.pocketcubesolver

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.transition.ChangeTransform
import android.transition.Explode
import android.transition.Slide
import android.view.MenuItem
import android.view.Window
import java.io.Serializable

abstract class DrawerLayoutActivity : CommonActivity() {

    private lateinit var toggle: ActionBarDrawerToggle

    protected abstract val toolbarUsed: Toolbar
    protected abstract val layoutResIdUsed: Int
    protected abstract val drawerLayoutUsed: DrawerLayout
    protected abstract val navigationViewUsed: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(this.layoutResIdUsed)
        this.setSupportActionBar(this.toolbarUsed)

        this.toggle = ActionBarDrawerToggle(this, this.drawerLayoutUsed, R.string.open, R.string.close)
        this.drawerLayoutUsed.addDrawerListener(this.toggle)
        this.toggle.syncState()

        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.navigationViewUsed.setNavigationItemSelectedListener { item ->
            when(item.itemId) {
                R.id.ai -> { createAndRunIntent(CubeSolverActivity::class.java) }
                R.id.solve -> { createAndRunIntent(SolveYourselfActivity::class.java) }
                R.id.camera -> { createAndRunIntent(CubeScannerActivity::class.java) }
                R.id.bluetooth -> { createAndRunIntent(BluetoothActivity::class.java) }
                else -> return@setNavigationItemSelectedListener true
            }
            return@setNavigationItemSelectedListener true
        }
    }

    protected fun createAndRunIntent(cls: Class<*>, vararg extras: Pair<String, Serializable>) {
        if (this.javaClass == cls) {
            hideDrawer()
            return
        }

        val intent = Intent(this, cls)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        for ((name, value) in  extras)
            intent.putExtra(name, value)

        hideDrawer()
        this.startActivity(intent)
        this.finish()
        this.overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(this.toggle.onOptionsItemSelected(item))
            return true

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isDrawerOpen()) {
            hideDrawer()
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        this.drawerLayoutUsed.closeDrawer(GravityCompat.START)
    }

    fun isDrawerOpen(): Boolean = this.drawerLayoutUsed.isDrawerOpen(GravityCompat.START)

    fun hideDrawer() = this.drawerLayoutUsed.closeDrawer(GravityCompat.START)
}