package com.procyk.maciej.android.pocketcubesolver

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.widget.ArrayAdapter
import android.widget.Button
import com.procyk.maciej.android.pocketcubesolver.cube.adapter.applyCurrentViewFor
import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.solver.DFSSolver
import com.procyk.maciej.android.pocketcubesolver.cube.solver.ICubeSolver
import com.procyk.maciej.android.pocketcubesolver.utils.BLUETOOTH_PERMISSIONS
import com.procyk.maciej.android.pocketcubesolver.utils.askForPermissions
import com.procyk.maciej.android.pocketcubesolver.utils.hasPermissions
import kotlinx.android.synthetic.main.activity_bluetooth.*
import kotlinx.android.synthetic.main.activity_cube_solver.drawerLayout
import kotlinx.android.synthetic.main.activity_cube_solver.navigationView
import kotlinx.android.synthetic.main.activity_cube_solver.toolbar
import kotlinx.android.synthetic.main.cube_layout.*
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.collections.ArrayList
import kotlin.math.min

val EXECUTOR_SERVICE = Executors.newCachedThreadPool()

class BluetoothActivity : DrawerLayoutActivity() {

    companion object {
        private val PERMISSIONS = BLUETOOTH_PERMISSIONS
    }

    private val cube: Cube = Cube.getEmpty()

    private val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    private lateinit var handler: Handler

    private val cubeButtons: ArrayList<Button> = ArrayList()

    override val drawerLayoutUsed: DrawerLayout
        get() = this.drawerLayout
    override val layoutResIdUsed: Int
        get() = R.layout.activity_bluetooth
    override val navigationViewUsed: NavigationView
        get() = this.navigationView
    override val toolbarUsed: Toolbar
        get() = this.toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cubeButtons.addAll(arrayOf(
            button0,button1,button2,button3,
            button4,button5,button6,button7,
            button8,button9,button10,button11,
            button12,button13,button14,button15,
            button16,button17,button18,button19,
            button20,button21,button22,button23
        ))

        this.refreshCubeView()
        this.buttonBluetoothSearch.setOnClickListener {
                bluetoothAdapter?.let { EXECUTOR_SERVICE.submit(SearchDevicesThread(it, handler)) }
                    ?: toastMessage(getString(R.string.enableBluetoothSend))
            }

        this.buttonBluetoothConnect.setOnClickListener { processData() }
    }

    fun setCubeData(data: ByteArray) {
        this.cube.copyFrom(Cube.fromBytes(data))
        this.refreshCubeView()
    }

    private fun refreshCubeView() {
        this.cube.applyCurrentViewFor(this.cubeButtons)
    }

    private fun processData() {
        bluetoothAdapter?.let {
            EXECUTOR_SERVICE.submit(ProcessDataThread(it, spinnerBluetoothDevices.selectedItem?.toString() ?: "", handler)) }
            ?: toastMessage(getString(R.string.enableBluetoothSend))
    }

    private fun updateSpinner(data: Array<*>) {
        data.filterIsInstance<String>().let {
            val dataAdapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, it)
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerBluetoothDevices.adapter = dataAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        if (!this.hasPermissions(PERMISSIONS))
            this.askForPermissions(PERMISSIONS)
    }

    override fun onResume() {
        super.onResume()
        handler = Handler { msg ->
            val data = msg.obj
            when(data) {
                is String -> this.toastMessage(data)
                is ByteArray -> this.setCubeData(data)
                is Array<*> -> this.updateSpinner(data)
            }
            return@Handler true
        }
    }
}

class SearchDevicesThread(private val btAdapter: BluetoothAdapter,
                          private val handler: Handler): Runnable {
    override fun run() {
        val devices = btAdapter.bondedDevices.map { it.address } .filterNotNull().toTypedArray()
        handler.sendObject(devices)
    }
}

class ProcessDataThread(private val btAdapter: BluetoothAdapter,
                        private val deviceAddress: String,
                        private val handler: Handler): Runnable {
    override fun run() {
        val connectThread = ConnectBluetooth(btAdapter.getRemoteDevice(deviceAddress))
        try {
            EXECUTOR_SERVICE.submit(connectThread).get()?.let {socket ->
                val process = DataProcess(socket, handler)
                EXECUTOR_SERVICE.submit(process).get()?.let {
                    socket.close()
                }
                handler.sendObject(Cube.getEmpty().toByteArray())
            }
        } catch (exc: Exception) { }
    }

}

class ConnectBluetooth(private val btDevice: BluetoothDevice) : Callable<BluetoothSocket?> {

    companion object {
        private val UUID_SERIAL = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    }

    override fun call(): BluetoothSocket? {
        var socket: BluetoothSocket? = null
        try {
            socket = btDevice.createRfcommSocketToServiceRecord(ConnectBluetooth.UUID_SERIAL)
            socket?.connect()
        } catch (exc: IOException) { }
        return socket
    }
}

class DataProcess(btSocket: BluetoothSocket, private val handler: Handler) : Thread() {

    companion object {
        const val DATA_BEGIN: Byte = '*'.toByte()
        const val DATA_END: Byte = '#'.toByte()

        private fun getDataFromBuffer(buffer: ByteArray): ByteArray {
            var b = buffer.size
            var e = buffer.size
            for (i in buffer.indices)
                if (buffer[i] == DATA_BEGIN) b = min(b, i)
            for (i in buffer.indices)
                if (buffer[i] == DATA_END) e = if (i > b) min(e, i) else e
            require(b < e && b != buffer.size && e != buffer.size) { "Bad data to extract from" }
            return ByteArray(e-b-1) { i -> buffer[i+b+1] }
        }
    }

    private var outputStream: OutputStream? = null
    private var inputStream: InputStream? = null

    private val solver: ICubeSolver = DFSSolver()

    init {
        try {
            outputStream = btSocket.outputStream
            inputStream = btSocket.inputStream
        } catch (exc: IOException) { }
    }

    override fun run() {
        val buffer = ByteArray((2+Cube.CUBE_FIELDS) * 2)

        inputStream?.read(buffer, 0, (2+Cube.CUBE_FIELDS) * 2)

        val bufferData = getDataFromBuffer(buffer)
        val cube = Cube.fromBytes(bufferData)
        handler.sendObject("Received data from NXT")
        handler.sendObject(bufferData)
        val moves = solver.findSolution(cube)
        for (i in 1..5) {
            outputStream?.write(byteArrayOf(DATA_BEGIN))
            outputStream?.write(ByteArray(moves.size) { moves[it].toByte() } )
            outputStream?.write(byteArrayOf(DATA_END))
        }
        handler.sendObject("Data sent to NXT")
        inputStream?.close()
        outputStream?.close()

    }
}

fun Handler.sendObject(data: Any) {
    val msg = Message()
    msg.obj = data
    this.sendMessage(msg)
}