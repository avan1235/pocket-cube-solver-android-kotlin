package com.procyk.maciej.android.pocketcubesolver

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.Toast
import com.procyk.maciej.android.pocketcubesolver.camera.CubeFaceRecognizer
import com.procyk.maciej.android.pocketcubesolver.camera.FaceNotDetectedException
import com.procyk.maciej.android.pocketcubesolver.camera.Photo
import com.procyk.maciej.android.pocketcubesolver.camera.SizedCubeFaceRecognizer
import com.procyk.maciej.android.pocketcubesolver.cube.model.Cube
import com.procyk.maciej.android.pocketcubesolver.cube.model.CubeFace
import com.procyk.maciej.android.pocketcubesolver.utils.*
import io.fotoapparat.Fotoapparat
import io.fotoapparat.log.loggers
import io.fotoapparat.log.none
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.selector.back
import kotlinx.android.synthetic.main.activity_cube_scanner.*
import kotlinx.android.synthetic.main.activity_cube_solver.*
import kotlinx.android.synthetic.main.activity_cube_solver.drawerLayout
import kotlinx.android.synthetic.main.activity_cube_solver.navigationView
import kotlinx.android.synthetic.main.activity_cube_solver.toolbar
import java.io.IOException
import java.io.InputStream
import java.io.InterruptedIOException
import java.io.OutputStream
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import kotlin.collections.ArrayList

class CubeScannerActivity : DrawerLayoutActivity() {

    companion object {
        private val PERMISSIONS = CAMERA_PERMISSIONS
    }

    private val cube = Cube.getEmpty()
    private lateinit var fotoaparat: Fotoapparat
    private var scanningFace = CubeFace.FRONT
    private val progress = ProgressState(4, 100, 6)
    private val cubeFaceRecognizer: CubeFaceRecognizer = SizedCubeFaceRecognizer()


    override val drawerLayoutUsed: DrawerLayout
        get() = this.drawerLayout
    override val layoutResIdUsed: Int
        get() = R.layout.activity_cube_scanner
    override val navigationViewUsed: NavigationView
        get() = this.navigationView
    override val toolbarUsed: Toolbar
        get() = this.toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.buttonTakePhoto.setOnClickListener { scanNextFace() }
        this.buttonSendCube.setOnClickListener {
            if (progress.finished)
                this.createAndRunIntent(CubeSolverActivity::class.java, "CUBE_TO_SOLVE" to this.cube.toIntArray())
            else
                toastMessage(getString(R.string.scanFullyCube))
        }
        this.fotoaparat = constructAparat()
        this.updateProgressBar()
    }

    private fun constructAparat(): Fotoapparat = Fotoapparat(
        context = this,
        view = this.camera_view,
        scaleType = ScaleType.CenterCrop,
        lensPosition = back(),
        logger = loggers(none())
    )

    private fun updateProgressBar() {
        this.progressBar.progress = this.progress.status
    }

    private fun scanNextFace() {
        this.fotoaparat.takePicture().toBitmap().whenAvailable {
            it?.let {
                if (progress.finished)
                    return@whenAvailable

                val bitmap = it.bitmap
                val inputPhoto = Photo(bitmap, isCentered = true)
                try {
                    val scannedFace = cubeFaceRecognizer.scanFace(inputPhoto)
                    this.nextFace.text = scanningFace.getToNextDirection().toString()
                    this.cube.setSingleFace(scannedFace, scanningFace.indexStart(), scanningFace.getFaceModShift())
                    scanningFace = scanningFace.nextScanFace()
                    progress.increaseStatus()
                    this.updateProgressBar()
                    toastMessage(scannedFace.toString(), duration = Toast.LENGTH_LONG)
                } catch (e: FaceNotDetectedException) {
                    toastMessage(getString(R.string.scanAgainToast))
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (!this.hasPermissions(PERMISSIONS))
            this.askForPermissions(PERMISSIONS)
        else
            this.fotoaparat.start()
    }

    override fun onStop() {
        super.onStop()
        this.fotoaparat.stop()
    }
}

