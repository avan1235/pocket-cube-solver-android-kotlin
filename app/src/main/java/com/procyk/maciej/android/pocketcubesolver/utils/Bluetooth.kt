package com.procyk.maciej.android.pocketcubesolver.utils

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.support.v7.app.AppCompatActivity

fun BluetoothAdapter.askToEnableIfRequired(activity: AppCompatActivity) {
    if (!this.isEnabled) {
        val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        activity.startActivityForResult(enableIntent, 1)
    }
}


