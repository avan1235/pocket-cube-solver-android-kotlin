package com.procyk.maciej.android.pocketcubesolver.cube.model

import java.lang.IllegalStateException
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.HashSet

class Cube private constructor() {
    companion object {
        const val CUBE_FIELDS = 24

        private val CORNERS_GROUPS: List<List<Int>> =
            listOf(
                listOf(10,1,4),
                listOf(11,0,20),
                listOf(2,7,13),
                listOf(3,12,21),
                listOf(9,5,18),
                listOf(23,8,19),
                listOf(22,15,16),
                listOf(14,17,6)
            )

        private val CORRECT_COLOURS_CORNERS: Set<Set<CubeColor>> =
            setOf(
                setOf(CubeColor.WHITE, CubeColor.ORANGE, CubeColor.GREEN),
                setOf(CubeColor.WHITE, CubeColor.ORANGE, CubeColor.BLUE),
                setOf(CubeColor.WHITE, CubeColor.GREEN, CubeColor.RED),
                setOf(CubeColor.WHITE, CubeColor.BLUE, CubeColor.RED),
                setOf(CubeColor.YELLOW, CubeColor.ORANGE, CubeColor.GREEN),
                setOf(CubeColor.YELLOW, CubeColor.ORANGE, CubeColor.BLUE),
                setOf(CubeColor.YELLOW, CubeColor.GREEN, CubeColor.RED),
                setOf(CubeColor.YELLOW, CubeColor.BLUE, CubeColor.RED)
            )

        fun getSolved(): Cube {
            val cube = Cube()
            cube.initSolvedFields()
            return cube
        }

        fun getEmpty(): Cube {
            return Cube()
        }

        fun fromBytes(array: ByteArray): Cube {
            require(array.size == CUBE_FIELDS)
            val cube = getEmpty()
            for ((index, value) in array.withIndex())
                cube.setColor(index, CubeColor.fromByte(value))
            return cube
        }
    }

    private val cubeColors: Array<CubeColor> = Array(CUBE_FIELDS) { CubeColor.NOT_SET }

    fun setColor(cubePosition: Int, color: CubeColor): Unit {
        this.cubeColors[cubePosition] = color
    }

    fun toIntArray(): IntArray = IntArray(CUBE_FIELDS) { index -> this.getColorAt(index).ordinal }

    fun toByteArray(): ByteArray = ByteArray(CUBE_FIELDS) { index -> this.getColorAt(index).ordinal.toByte() }

    fun fromIntArray(array: IntArray): Unit {
        require(array.size == CUBE_FIELDS)
        for ((index, value) in array.withIndex())
            this.setColor(index, CubeColor.fromOrdinal(value))
    }

    fun shuffle(changes: Int = 15): Unit {
        for (i in 0..changes)
            this.moveRandomly()
    }

    fun resetFields() {
        for(i in 0 until CUBE_FIELDS)
            this.setColor(i, CubeColor.NOT_SET)
    }

    fun setSolved(): Unit = this.initSolvedFields()

    private fun isFilled(): Boolean = cubeColors.filter { it != CubeColor.NOT_SET }.size == cubeColors.size

    private fun getColorsForCorner(corner: Collection<Int>): Set<CubeColor> {
        assert(corner.size == 3)
        val result = HashSet<CubeColor>()
        for (num in corner)
            result.add(this.getColorAt(num))
        return result
    }

    fun setSingleFace(colors: List<CubeColor>, start: Int, shift: Int = 0) {
        val indexes = listOf(start, start+1, start+2, start+3)
        for ((index, color) in colors.withIndex()) {
            this.setColor(indexes[(index + shift) % 4], color)
        }
    }

    fun isCorrectlyFilled(): Boolean {
        if (!isFilled()) return false
        val occurrencesSet: MutableSet<Set<CubeColor>> = HashSet()
        for (cornerNumbers in CORNERS_GROUPS)
            occurrencesSet.add(getColorsForCorner(cornerNumbers))
        return occurrencesSet == CORRECT_COLOURS_CORNERS
    }

    private fun getCubeStateRepresentation(): String {
        if (!isFilled()) throw IllegalStateException("Cannot get representation for not filled cube")
        val states = IntArray(cubeColors.size) { i -> cubeColors[i].ordinal }
        val positions = IntArray(CubeColor.values().size) { -1 }
        var index = 0
        states.forEach { state -> if (positions[state] == -1) positions[state] = index++ }
        val builder = StringBuilder()
        states.forEach { state -> builder.append(positions[state]) }
        return builder.toString()
    }

    fun move(move: CubeMove): Unit {
        for (perm in move.permutations)
            this.applyPermutation(perm)
    }

    fun isSolved(): Boolean {
        for (i in (0 until 24).step(4)) {
            for (j in 1 until 4)
                if (cubeColors[i] != cubeColors[i+j])
                    return false
        }
        return true
    }

    fun moveRandomly(): Unit {
        val possibleMoves = CubeMove.values()
        this.move(possibleMoves[(0 until possibleMoves.size).random()])
    }

    fun copy(): Cube {
        val returnCube = Cube()
        this.cubeColors.forEachIndexed {index, cubeColor ->  returnCube.cubeColors[index] = cubeColor}
        return returnCube
    }

    fun copyFrom(cube: Cube) {
        for ((i, color) in cube.cubeColors.withIndex())
            this.setColor(i, color)
    }

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other?.javaClass != this.javaClass) return false
        other as Cube
        if (!Arrays.equals(this.cubeColors, other.cubeColors)) return false
        return true
    }

    override fun hashCode(): Int = Arrays.hashCode(this.cubeColors)

    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("           ---- ----           ").append(System.lineSeparator())
        builder.append("          | %2s | %2s |          ".format(cubeColors[8].toString(),cubeColors[9].toString())).append(System.lineSeparator())
        builder.append("          |    |    |          ").append(System.lineSeparator())
        builder.append("           ---- ----           ").append(System.lineSeparator())
        builder.append("          | %2s | %2s |          ".format(cubeColors[11].toString(),cubeColors[10].toString())).append(System.lineSeparator())
        builder.append("          |    |    |          ").append(System.lineSeparator())
        builder.append(" ---- ---- ---- ---- ---- ---- ").append(System.lineSeparator())
        builder.append("| %2s | %2s | %2s | %2s | %2s | %2s |".format(cubeColors[23].toString(),cubeColors[20].toString(),cubeColors[0].toString(),cubeColors[1].toString(),cubeColors[4].toString(),cubeColors[5].toString())).append(System.lineSeparator())
        builder.append("|    |    |    |    |    |    |").append(System.lineSeparator())
        builder.append(" ---- ---- ---- ---- ---- ---- ").append(System.lineSeparator())
        builder.append("| %2s | %2s | %2s | %2s | %2s | %2s |".format(cubeColors[22].toString(),cubeColors[21].toString(),cubeColors[3].toString(),cubeColors[2].toString(),cubeColors[7].toString(),cubeColors[6].toString())).append(System.lineSeparator())
        builder.append("|    |    |    |    |    |    |").append(System.lineSeparator())
        builder.append(" ---- ---- ---- ---- ---- ---- ").append(System.lineSeparator())
        builder.append("          | %2s | %2s |          ".format(cubeColors[12].toString(),cubeColors[13].toString())).append(System.lineSeparator())
        builder.append("          |    |    |          ").append(System.lineSeparator())
        builder.append("           ---- ----           ").append(System.lineSeparator())
        builder.append("          | %2s | %2s |          ".format(cubeColors[15].toString(),cubeColors[14].toString())).append(System.lineSeparator())
        builder.append("          |    |    |          ").append(System.lineSeparator())
        builder.append("           ---- ----           ").append(System.lineSeparator())
        builder.append("          | %2s | %2s |          ".format(cubeColors[16].toString(),cubeColors[17].toString())).append(System.lineSeparator())
        builder.append("          |    |    |          ").append(System.lineSeparator())
        builder.append("           ---- ----           ").append(System.lineSeparator())
        builder.append("          | %2s | %2s |          ".format(cubeColors[19].toString(),cubeColors[18].toString())).append(System.lineSeparator())
        builder.append("          |    |    |          ").append(System.lineSeparator())
        builder.append("           ---- ----           ").append(System.lineSeparator())
        return builder.toString()
    }

    private fun initSolvedFields() {
        this.setColor(0, CubeColor.ORANGE)
        this.setColor(1, CubeColor.ORANGE)
        this.setColor(2, CubeColor.ORANGE)
        this.setColor(3, CubeColor.ORANGE)
        this.setColor(4, CubeColor.GREEN)
        this.setColor(5, CubeColor.GREEN)
        this.setColor(6, CubeColor.GREEN)
        this.setColor(7, CubeColor.GREEN)
        this.setColor(8, CubeColor.WHITE)
        this.setColor(9, CubeColor.WHITE)
        this.setColor(10, CubeColor.WHITE)
        this.setColor(11, CubeColor.WHITE)
        this.setColor(12, CubeColor.YELLOW)
        this.setColor(13, CubeColor.YELLOW)
        this.setColor(14, CubeColor.YELLOW)
        this.setColor(15, CubeColor.YELLOW)
        this.setColor(16, CubeColor.RED)
        this.setColor(17, CubeColor.RED)
        this.setColor(18, CubeColor.RED)
        this.setColor(19, CubeColor.RED)
        this.setColor(20, CubeColor.BLUE)
        this.setColor(21, CubeColor.BLUE)
        this.setColor(22, CubeColor.BLUE)
        this.setColor(23, CubeColor.BLUE)
    }

    fun getColorAt(index: Int): CubeColor = cubeColors[index]

    fun toBytes(): ByteArray = ByteArray(CUBE_FIELDS) { index -> this.cubeColors[index].toByte() }

    private fun applyPermutation(permutation: IntArray) {
        val first = this.cubeColors[permutation.first()]
        for (i in 0 until permutation.size-1)
            cubeColors[permutation[i]] = cubeColors[permutation[i+1]]
        cubeColors[permutation.last()] = first
    }
}