package com.procyk.maciej.android.pocketcubesolver.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.procyk.maciej.android.pocketcubesolver.CubeScannerActivity

val CAMERA_PERMISSIONS = arrayOf(
    android.Manifest.permission.CAMERA)

val MEMORY_PERMISSIONS = arrayOf(
    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    android.Manifest.permission.READ_EXTERNAL_STORAGE)


val BLUETOOTH_PERMISSIONS = arrayOf(
    android.Manifest.permission.BLUETOOTH,
    android.Manifest.permission.BLUETOOTH_ADMIN)

fun String.isPermissionGranted(context: Context)
        = ContextCompat.checkSelfPermission(context, this) == PackageManager.PERMISSION_GRANTED

fun Context.hasPermissions(permissions: Array<String>)
        = permissions
            .filter { !it.isPermissionGranted(this) }
            .isEmpty()

fun Activity.askForPermissions(permissions: Array<String>)
        = ActivityCompat.requestPermissions(this, permissions,0)