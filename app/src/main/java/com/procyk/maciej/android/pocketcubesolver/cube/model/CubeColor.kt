package com.procyk.maciej.android.pocketcubesolver.cube.model

enum class CubeColor {
    WHITE,
    RED,
    ORANGE,
    BLUE,
    GREEN,
    YELLOW,
    NOT_SET;

    override fun toString(): String =
        when(this) {
            NOT_SET -> ""
            else -> super.toString().substring(0,1)
        }

    companion object {
        fun fromOrdinal(ord: Int): CubeColor = values()[ord]

        fun definedColors(): Set<CubeColor> {
            val colors = mutableSetOf<CubeColor>()
            colors.addAll(values())
            colors.remove(NOT_SET)
            return colors
        }

        fun fromByte(ord: Byte): CubeColor = fromOrdinal(ord.toInt())
    }

    fun toByte() =  this.ordinal.toByte()
}